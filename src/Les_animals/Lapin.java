package Les_animals;

public class Lapin extends Animal{

	private String prenom;
	private String race;
	private String couleur;
	
	public Lapin(String prenom, String race, String couleur) {
		super();
		this.prenom = prenom;
		this.race = race;
		this.couleur = couleur;
	}

	public Lapin() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Lapin [prenom=" + prenom + ", race=" + race + ", couleur=" + couleur + "]";
	}
	
	
	
}
