package Les_animals;

import java.util.ArrayList;
import java.util.List;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Animal mougoy = new Lapin("Mougoy", "blanc", "blanc");
		mougoy.crier();

		
		List<Animal> lesAnimals = new ArrayList<Animal>();
		lesAnimals.add(new Chien("Raoul", "Dog argentin", "Gris argent�"));
		lesAnimals.add(new Chien("Dede", "Ratete de Tobrouk", "Noir"));
		lesAnimals.add(new Chat("Miou", "Batard", "Sale"));
		lesAnimals.add(new Chat("Minou", "Egyptien", "Gris"));
		lesAnimals.add(new Lapin("Civet", "Batard", "Sale"));
		lesAnimals.add(new Lapin("Ragout", "Egyptien", "Gris"));
		
		
//		System.out.println("affichage type 2");
//		lesAnimals.forEach(System.out::println); // prog fonctionnel java 8
		
		for(int i = 0;i<lesAnimals.size();i++) {
			(lesAnimals.get(i)).crier();
		}
		
	}

}
