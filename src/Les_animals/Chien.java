package Les_animals;

public class Chien extends Animal implements ICrie{
	
	private String prenom;
	private String race;
	private String couleur;

	public void crier() {
		System.out.println("Wouf wouf !");
	}

	public Chien(String prenom, String race, String couleur) {
		super();
		this.prenom = prenom;
		this.race = race;
		this.couleur = couleur;
	}

	public Chien() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Chien [prenom=" + prenom + ", race=" + race + ", couleur=" + couleur + "]";
	}

	
	
}
