package Les_animals;

public class Chat extends Animal implements ICrie{

	private String prenom;
	private String race;
	private String couleur;

	public void crier() {
		System.out.println("Miaou miaou !");
	}

	public Chat(String prenom, String race, String couleur) {
		super();
		this.prenom = prenom;
		this.race = race;
		this.couleur = couleur;
	}

	public Chat() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Chat [prenom=" + prenom + ", race=" + race + ", couleur=" + couleur + "]";
	}
	
	
	
}
