package poo_J1;

import java.util.ArrayList;
import java.util.List;

public class Poo1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Voiture v1 = new Voiture("DD928AB", "FIAT", "Panda", 4, "Noire");
		Voiture v2 = new Voiture(v1.getNumeroImmat(), v1.getMarque(), v1.getModele(), v1.getPuissanceFiscale(),
				v1.getCouleur());
		// v2 = v1;

		List<Voiture> mesvoitures = new ArrayList<Voiture>();
			mesvoitures.add(new Voiture("EB520FF", "Peugeot", "106", 7, "Bleu"));
			mesvoitures.add(new Voiture("952AWF06", "Porsche", "928", 32, "Bleu"));
			mesvoitures.add(new Voiture("123FG06", "Opel", "Kadett", 11, "Jaune"));
			mesvoitures.add(v1);

		v1.setCouleur("Grise");
		v1.setPuissanceFiscale(5 + v1.getPuissanceFiscale());

		System.out.println(v1);
		System.out.println(v2);

		System.out.println("affichage type 1");
		mesvoitures.forEach(Voiture -> System.out.println(Voiture));

		System.out.println("affichage type 2");
		mesvoitures.forEach(System.out::println); // prog fonctionnel java 8

		System.out.println("affichage type 3");
		for (Voiture v : mesvoitures) {
			System.out.println(v);
		}
	}

}
