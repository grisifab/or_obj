package poo_J1;

public class Voiture {
	private String numeroImmat;
	private String marque;
	private String modele;
	private int puissanceFiscale;
	private String couleur;
	
	public Voiture(String numeroImmat, String marque, String modele, int puissanceFiscale, String couleur) {
		super();
		this.numeroImmat = numeroImmat;
		this.marque = marque;
		this.modele = modele;
		this.puissanceFiscale = puissanceFiscale;
		this.couleur = couleur;
	}

	public Voiture() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getNumeroImmat() {
		return numeroImmat;
	}

	public void setNumeroImmat(String numeroImmat) {
		this.numeroImmat = numeroImmat;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getPuissanceFiscale() {
		return puissanceFiscale;
	}

	public void setPuissanceFiscale(int puissanceFiscale) {
		this.puissanceFiscale = puissanceFiscale;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	@Override
	public String toString() {
		return "Voiture [numeroImmat=" + numeroImmat + ", marque=" + marque + ", modele=" + modele
				+ ", puissanceFiscale=" + puissanceFiscale + ", couleur=" + couleur + "]";
	}

}



