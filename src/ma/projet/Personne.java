package ma.projet;

public abstract class Personne {
	
	private int id;
	private String nom;
	private String prenom;
	private String mail;
	private String telephone;
	private double salaire;
	private static int count;
	
	public abstract void calculerSalaire(); // c'est l� que les bact�ries attaquent !
	
	public abstract void afficherInfos(); // c'est l� que les bact�ries attaquent !
	
	public Personne(String nom, String prenom, String mail, String telephone, double salaire) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephone = telephone;
		this.salaire = salaire;
	}

	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", telephone="
				+ telephone + ", salaire=" + salaire + "]";
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}
	
	
	

}
