package ma.projet;

import ma.projet.bean.Developpeur;
import ma.projet.bean.Manager;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Personne Dev1 = new Developpeur("Zeruk", "Victoria", "victoria.zeruck@gmail.com", "0645857899", 10000, "Sites Italo-Russes");
		Personne Dev2 = new Developpeur("Jesaispas", "Asma", "asma.jesaispas@gmail.com", "0645857811", 10000, "Sites Italo-Tunisiens");
		
		Personne Man1 = new Manager("Lembrouille", "Didier", "didier.lembrouille@gmail.cum", "pas de telephone", 12000, "baston");
		Personne Man2 = new Manager("Lembrouille", "Fernand", "fernand.lembrouille@gmail.cum", "pas de telephone", 18000, "bagarre");

		System.out.println(Dev1);
		Dev1.afficherInfos();
		Dev1.calculerSalaire();
		Dev1.afficherInfos();
		System.out.println("---------");
		
		System.out.println(Dev2);
		Dev2.afficherInfos();
		Dev2.calculerSalaire();
		Dev2.afficherInfos();
		System.out.println("---------");
		
		System.out.println(Man1);
		Man1.afficherInfos();
		Man1.calculerSalaire();
		Man1.afficherInfos();
		System.out.println("---------");
		
		System.out.println(Man2);
		Man2.afficherInfos();
		Man2.calculerSalaire();
		Man2.afficherInfos();
		System.out.println("---------");
		
	}

}
