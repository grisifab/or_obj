package ma.projet.bean;

import ma.projet.Personne;

public class Developpeur extends Personne {
	
	private String specialite;

	@Override
	public void calculerSalaire() {
		// TODO Auto-generated method stub
		this.setSalaire(this.getSalaire()*1.2);
	}
	
	@Override
	public void afficherInfos() {
		// TODO Auto-generated method stub
		System.out.println("Le salaire du développeur " + this.getNom() + " " + this.getPrenom() + " est : " + this.getSalaire() + " euros, sa spécialité : " + this.getSpecialite());
	}

	public Developpeur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Developpeur(String nom, String prenom, String mail, String telephone, double salaire, String specialite) {
		super(nom, prenom, mail, telephone, salaire);
		this.specialite = specialite;
	}

	@Override
	public String toString() {
		return super.toString() + "Developpeur [specialite=" + specialite + "]";
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

}
