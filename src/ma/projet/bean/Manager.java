package ma.projet.bean;

import ma.projet.Personne;

public class Manager extends Personne {
	
	private String service;

	@Override
	public void calculerSalaire() {
		// TODO Auto-generated method stub
		this.setSalaire(this.getSalaire()*1.35);
	}
	
	@Override
	public void afficherInfos() {
		// TODO Auto-generated method stub
		System.out.println("Le salaire du manageur " + this.getNom() + " " + this.getPrenom() + " est : " + this.getSalaire() + " euros, son service : " + this.getService());
	}

	public Manager(String nom, String prenom, String mail, String telephone, double salaire, String service) {
		super(nom, prenom, mail, telephone, salaire);
		this.service = service;
	}

	public Manager() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + "Manager [service=" + service + "]";
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

}
