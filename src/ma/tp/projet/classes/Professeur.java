package ma.tp.projet.classes;

import java.util.Date;

public class Professeur extends Employe {
	
	private String specialite;

	public Professeur(String nom, String prenom, Date dateNaissance, float salaire, String specialite) {
		super(nom, prenom, dateNaissance, salaire);
		this.specialite = specialite;
	}

	public Professeur() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + "Professeur [specialite=" + specialite + "]";
	}

	
	
	

}
