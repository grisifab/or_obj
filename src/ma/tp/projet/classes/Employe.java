package ma.tp.projet.classes;

import java.util.Date;

public class Employe extends Personne {
	
	private float salaire;

	public Employe(String nom, String prenom,Date dateNaissance, float salaire) {
		super(nom, prenom, dateNaissance);
		this.salaire = salaire;
	}

	public Employe() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + "Employe [salaire=" + salaire + "]";
	}

	
	

}
