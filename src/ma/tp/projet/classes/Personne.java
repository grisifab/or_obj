package ma.tp.projet.classes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Personne {
	
	private int id;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private static int count;
	
	

	public Personne(String nom, String prenom, Date dateNaissance) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
	}

	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		
//		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE);
//		String date = df.format(dateNaissance);
//		OU
//		SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy");
//		String date = dateFormat.format(dateNaissance);
// 		OU
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(dateNaissance);
		
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + date + "]";
	}

	
	
	
}
