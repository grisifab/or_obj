package ma.tp.projet.classes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//1. D�velopper les classes dans le package "ma.tp.projet.classes". 
//o Chaque classe doit contenir un constructeur d'initialisation. 
//o Chaque classe doit red�finir la m�thode toString(). 
//2. D�velopper une classe de Application dans le package "ma.tp.projet.test", 
//  dans cette classe on demande de cr�er : 
//	o deux �tudiants. 
//	o deux employ�s. 
//	o deux professeurs. 
//	o afficher les informations de chaque personne.


public class poo_J5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		List<Personne> lesGens = new ArrayList<Personne>();
		
		lesGens.add(new Etudiant("Clire","Marie",new Date("1976/07/24")," 65678754 "));
		
		lesGens.add(new Etudiant("Doe","Thomas",new Date("1976/07/24"),"87543543"));
		
		lesGens.add(new Employe("Baton","Jean",new Date("1976/07/24"),1500));

		lesGens.add(new Employe("Garros","Roland",new Date("1976/07/24"),1000));
		
		lesGens.add(new Professeur("Frey","Kevin",new Date("1976/07/24"),5700,"Java.JEE"));
		
		lesGens.add(new Professeur("Dupont","Mathieu",new Date("1976/07/24"),5000,"Mathematique"));
		
//		System.out.println("affichage type 2");
//		lesGens.forEach(System.out::println); // prog fonctionnel java 8
		
		System.out.println("---------------------------------------------------");
		System.out.println("La liste des employes : ");
		System.out.println(" ");
		for (int i = 0; i<6; i++) {
			if(((lesGens.get(i)) instanceof Employe) && !((lesGens.get(i)) instanceof Professeur)) {
				System.out.println(lesGens.get(i));
			}
		}
		System.out.println("---------------------------------------------------");
		System.out.println("La liste des etudiants : ");
		System.out.println(" ");
		for (int i = 0; i<6; i++) {
			if((lesGens.get(i)) instanceof Etudiant) {
				System.out.println(lesGens.get(i));
			}
		}
		System.out.println("---------------------------------------------------");
		System.out.println("La liste des professeurs : ");
		System.out.println(" ");
		for (int i = 0; i<6; i++) {
			if((lesGens.get(i)) instanceof Professeur) {
				System.out.println(lesGens.get(i));
			}
		}
		
	}

}
