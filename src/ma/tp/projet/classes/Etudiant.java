package ma.tp.projet.classes;

import java.util.Date;

public class Etudiant extends Personne {

	private String cNE;

	public Etudiant(String nom, String prenom, Date dateNaissance, String cNE) {
		super(nom, prenom, dateNaissance);
		this.cNE = cNE;
	}

	public Etudiant() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + "Etudiant [cNE=" + cNE + "]";
	}

	
	
	

}
