package poo_J2;

public class Livre {
	
	private String title;
	private String writer;
	private String editor;
	private int numberOfPages;
	private float weight;
	
	public Livre(String title, String writer, String editor, int numberOfPages, double d) {
		super();
		this.title = title;
		this.writer = writer;
		this.editor = editor;
		this.numberOfPages = numberOfPages;
		this.weight = (float) d;
	}

	public Livre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "Livre [title=" + title + ", writer=" + writer + ", editor=" + editor + ", numberOfPages="
				+ numberOfPages + ", weight=" + weight + "]";
	}
	
	

	
	
}
