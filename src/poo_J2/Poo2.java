package poo_J2;

import java.util.ArrayList;
import java.util.List;


public class Poo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Livre L1 = new Livre("Gone with the wind","Alice Sapritch","Edition du plat pays",180,456.3);
		Livre L2 = new Livre("Guitou goes to Mykonos","Michounet","Les �ditions du Ionf",80,256.3);
		
		System.out.println(L1);
		System.out.println(L2);
		
		List<Livre> mesLivres = new ArrayList<Livre>();
		mesLivres.add(L1);
		mesLivres.add(L2);
		mesLivres.add(new Livre("Darla dirladada","PatricK S�bastien","Pouet pouet",20,56.3));
		mesLivres.add(new Livre("Franky goes to hollywood","Tonald Drump","Crosgon",2000,1056.3));
		mesLivres.add(new Livre("D�d� Pustule","Polansky","Edition du sud",100,356.3));
		
		System.out.println("affichage type 2");
		mesLivres.forEach(System.out::println); // prog fonctionnel java 8
		
	}

}
