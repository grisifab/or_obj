package bagnole;

public class Moteur {
	
	private int num;
	private float poids;
	
	public Moteur(int num, float poids) {
		super();
		this.num = num;
		this.poids = poids;
	}
	public Moteur() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Moteur [num=" + num + ", poids=" + poids + "]";
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public float getPoids() {
		return poids;
	}
	public void setPoids(float poids) {
		this.poids = poids;
	}

}
