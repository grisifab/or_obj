package bagnole;

public class Vehicule {
	
	private String numImmat;
	private String marque;
	private String mod�le;
	private final Chassis chassis;
	private Moteur moteur;
	
	public Vehicule(String numImmat, String marque, String mod�le, Chassis chassis, Moteur moteur) {
		super();
		this.numImmat = numImmat;
		this.marque = marque;
		this.mod�le = mod�le;
		this.chassis = chassis;
		this.moteur = moteur;
	}
	
	public String getNumImmat() {
		return numImmat;
	}
	
	public void setNumImmat(String numImmat) {
		this.numImmat = numImmat;
	}
	
	public String getMarque() {
		return marque;
	}
	
	public void setMarque(String marque) {
		this.marque = marque;
	}
	
	public String getMod�le() {
		return mod�le;
	}
	
	public void setMod�le(String mod�le) {
		this.mod�le = mod�le;
	}
	
	public Moteur getMoteur() {
		return moteur;
	}
	
	public void setMoteur(Moteur moteur) {
		this.moteur = moteur;
	}
	
	public Chassis getChassis() {
		return chassis;
	}

	@Override
	public String toString() {
		return "Vehicule [numImmat=" + numImmat + ", marque=" + marque + ", mod�le=" + mod�le + ", chassis=" + chassis
				+ ", moteur=" + moteur + "]";
	}

	
	
	

}
