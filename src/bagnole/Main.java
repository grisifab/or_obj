package bagnole;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Chassis chassis1 = new Chassis("WWW520FF","Alu");
		
		Moteur moteur = new Moteur(123,80);
		
		Vehicule v1 = new Vehicule("EB520FF", "Pijot", "106", chassis1, moteur);
		Vehicule v2 = new Vehicule("EB521FF", "Pijot", "206", chassis1, moteur);
		
		System.out.println(v1);
		System.out.println(v2);
		
		// pas possible de modifier le chassis en passant par vehicule v1.setMatiere("bois")
		// mais possible en passant par le chassis, si les set ne sont pas commentÚs

		chassis1.setMatiere("bois");
		
		System.out.println(v1);
	}

}
