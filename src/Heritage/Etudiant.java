package Heritage;


public class Etudiant extends Personne {
	
	private String niveau;

	public Etudiant(String nom, String prenom, String dateNaissance, Adresse adresse, Sport sport, String niveau) {
		super(nom, prenom, dateNaissance, adresse, sport);
		this.niveau = niveau;
	}

	public Etudiant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return super.toString() + "Etudiant [niveau=" + niveau + "]";
	}

	@Override
	public void afficherNomComplet() {
		// TODO Auto-generated method stub
		System.out.println("je suis un �tudiant");
	}



}
