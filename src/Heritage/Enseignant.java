package Heritage;

public class Enseignant extends Personne {
	
	private String dateEmbauche;
	private float salaire;
	private boolean greve;
	
	

	public Enseignant(String nom, String prenom, String dateNaissance, Adresse adresse, Sport sport, String dateEmbauche,
			float salaire, boolean greve) {
		super(nom, prenom, dateNaissance, adresse, sport);
		this.dateEmbauche = dateEmbauche;
		this.salaire = salaire;
		this.greve = greve;
	}

	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getDateEmbauche() {
		return dateEmbauche;
	}

	public void setDateEmbauche(String dateEmbauche) {
		this.dateEmbauche = dateEmbauche;
	}

	public float getSalaire() {
		return salaire;
	}

	public void setSalaire(float salaire) {
		this.salaire = salaire;
	}

	public boolean isGreve() {
		return greve;
	}

	public void setGreve(boolean greve) {
		this.greve = greve;
	}

	@Override
	public String toString() {
		return super.toString() + "Enseignant [dateEmbauche=" + dateEmbauche + ", salaire=" + salaire + ", greve=" + greve + "]";
	}

	@Override
	public void afficherNomComplet() {
		// TODO Auto-generated method stub
		System.out.println("je suis un enseignant");
	}
	
	

}
