package Heritage;

public class Personne implements IMiseEnForme {
	
	private String nom;
	private String prenom;
	private String dateNaissance;
	private Adresse adresse;
	private Sport sport;
	
	public Personne(String nom, String prenom, String dateNaissance, Adresse adresse, Sport sport) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.adresse = adresse;
		this.sport = sport;
	}
	public void afficherNomComplet() {
	}
	
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + ", adresse="
				+ adresse + ", sport=" + sport + "]";
	}
	
	@Override
	public void afficherNomMajuscule() {
		// TODO Auto-generated method stub
		System.out.println(this.nom.toUpperCase()); 
	}

	@Override
	public void afficherPrenomMajuscule() {
		// TODO Auto-generated method stub
		System.out.println(this.prenom.toUpperCase()); 
	}
	
	
}
