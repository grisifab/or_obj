package Heritage;

import java.util.ArrayList;
import java.util.List;

public class poo_J3 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Adresse toutLeMonde = new Adresse("Nice","rue de France", 6000);
		
		Personne et1 = new Etudiant("","","",toutLeMonde, Sport.TROUDEBALL, "Terminale");
		System.out.println(et1);
		et1.afficherNomComplet();
		
		Personne en1 = new Enseignant("","","",toutLeMonde, Sport.TROUDEBALL,"19_02_1984",2500,false);
		System.out.println(en1);
		en1.afficherNomComplet();
		
		Personne en2 = new Enseignant("dido","pipoo","moumou",toutLeMonde, Sport.TROUDEBALL,"19_02_1984",2500,false);
		System.out.println(en1);
		en2.afficherNomComplet();
		en2.afficherNomMajuscule();
		en2.afficherPrenomMajuscule();
		
		Personne bibou = new Enseignant("sbooby", "molle", "24_07_1976", toutLeMonde, Sport.TROUDEBALL,"01_01_2000", 5000, true);
		System.out.println(bibou);
		
//		Personne p1 = new Personne("Morice","Benguigui","24_08_1963");
//		System.out.println(p1);
		
		List<Personne> lesGens = new ArrayList<Personne>();
		lesGens.add(new Etudiant("Morice","Benguigui","24_08_1963",new Adresse("La Bourboule","rue de la touffe",38200), Sport.TROUDEBALL,"Premi�re"));
		lesGens.add(new Etudiant("Jean-Claude","Dus","25_08_1965",toutLeMonde, Sport.TROUDEBALL,"BTS"));
		lesGens.add(new Etudiant("Eva","PrendreCher","14_08_1983",toutLeMonde, Sport.TROUDEBALL,"Doctorant"));
		lesGens.add(new Enseignant("Vorice","Benguigui","24_08_1963",toutLeMonde, Sport.TROUDEBALL,"19_02_1984",2700,false));
		lesGens.add(new Enseignant("Cean-Jlaude","Dus","25_08_1965",toutLeMonde, Sport.TROUDEBALL,"19_02_1984",2800,false));
		lesGens.add(new Enseignant("Evanoe","PrendreCher","14_08_1983",toutLeMonde, Sport.TROUDEBALL,"19_02_1984",3500,false));
		lesGens.add(new Enseignant("Evonoe","PrendreCher","14_08_1983",toutLeMonde, Sport.TROUDEBALL,"19_02_1984",3500,false));
		
		System.out.println("affichage type 2");
		lesGens.forEach(System.out::println); // prog fonctionnel java 8
		
	}

}
