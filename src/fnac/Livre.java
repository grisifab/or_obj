package fnac;

public class Livre extends Article{

	private String isbn;
	private int nbPages;
	public Livre(String designation, float prix, String isbn, int nbPages) {
		super(designation, prix);
		this.isbn = isbn;
		this.nbPages = nbPages;
	}
	public Livre() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getNbPages() {
		return nbPages;
	}
	public void setNbPages(int nbPages) {
		this.nbPages = nbPages;
	}
	@Override
	public String toString() {
		return super.toString() + "Livre [isbn=" + isbn + ", nbPages=" + nbPages + "]";
	}
	
}
