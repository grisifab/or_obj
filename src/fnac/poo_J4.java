package fnac;

import java.util.ArrayList;
import java.util.List;

public class poo_J4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Article> monCaddy = new ArrayList<Article>();
		
		Article a1 = new Video("VHS Tortue Ninbja",12,120);
		monCaddy.add(a1);
		
		Article a2 = new Disque("Disque Master dfr",52,"bleu");
		monCaddy.add(a2);
		
		Article a3 = new Poche ("Le petit livre rouge",22,"XXYY",120,"connerie");
		monCaddy.add(a3);
		
		System.out.println("affichage type 2");
		monCaddy.forEach(System.out::println); // prog fonctionnel java 8
		
	   
	   for (int i = 0; i<3; i++) {
	   monCaddy.get(i).acheter();
	   }
	   

	}

}
