package fnac;

public class Video extends Article {

	private float duree;
	
	public void afficher() {
		
		
	}

	public Video(String designation, float prix, float duree) {
		super(designation, prix);
		this.duree = duree;
	}

	public Video() {
		super();
		// TODO Auto-generated constructor stub
	}

	public float getDuree() {
		return duree;
	}

	public void setDuree(float duree) {
		this.duree = duree;
	}

	@Override
	public String toString() {
		return super.toString() + "Video [duree=" + duree + "]";
	}
	
	
	
}
