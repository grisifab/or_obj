package fnac;

public class Poche extends Livre {
	
	private String categorie;

	public Poche(String designation, float prix, String isbn, int nbPages, String categorie) {
		super(designation, prix, isbn, nbPages);
		this.categorie = categorie;
	}

	public Poche() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		return super.toString() + "Poche [categorie=" + categorie + "]";
	}
	
	

}
