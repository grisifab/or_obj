package fnac;

public class Disque extends Article {
	
	private String label;
	
	public void ecouter() {
	
	}

	public Disque(String designation, float prix, String label) {
		super(designation, prix);
		this.label = label;
	}

	public Disque() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return super.toString() + "Disque [label=" + label + "]";
	}

	
	
	

}
