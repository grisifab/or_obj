package mercredi_15_janvier_exo1;

public class Profil {
	
	private int idp;
	private int code;
	private String Libelle;
	private static int count;
	
	public Profil(int code, String libelle) {
		super();
		this.idp = ++count;
		this.code = code;
		Libelle = libelle;
	}

	@Override
	public String toString() {
		return "Profil [idp=" + idp + ", code=" + code + ", Libelle=" + Libelle + "]";
	}

}
