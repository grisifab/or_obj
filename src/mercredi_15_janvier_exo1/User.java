package mercredi_15_janvier_exo1;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {
	
	private int idu;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private Profil profil;
	private static int count;
	
	public User(String nom, String prenom, Date dateNaissance, Profil profil) {
		super();
		this.idu = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.profil = profil;	
	}

	@Override
	public String toString() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(dateNaissance);
		
		return "User [idu=" + idu + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + date
				+ ", profil=" + profil + "]";
	}
}
