package mercredi_15_janvier_exo1.test;

import java.util.Date;

import mercredi_15_janvier_exo1.Profil;
import mercredi_15_janvier_exo1.User;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Cr�ation profils
		Profil p1 = new Profil(1, "CP");
		Profil p2 = new Profil(2, "MN");
		Profil p3 = new Profil(3, "DP");
		
		//Cr�ation utilisateurs
		User u1 = new User("Foutalacci", "Dominique", new Date("18/02/1926"), p2);
		User u2 = new User("Perdonoli", "Ange", new Date("18/02/1946"), p2);
		User u3 = new User("Merguez", "BBQ", new Date("18/02/1966"), p3);
		
		System.out.println(u1);
		System.out.println(u2);
		System.out.println(u3);

	}

}
