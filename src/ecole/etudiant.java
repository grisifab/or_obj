package ecole;

public class etudiant {
	
	private int ide;
	private String nom;
	private String prenom;
	private String dateNaissance;
	private Filiere filiere;
	private static int count;
	
	public etudiant(String nom, String prenom, String dateNaissance,
			Filiere filiere) {
		super();
		this.ide = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.filiere = filiere;
	}

	public int getIde() {
		return ide;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public String toString() {
		return "etudiant [ide=" + ide + ", nom=" + nom + ", prenom=" + prenom
				+ ", dateNaissance=" + dateNaissance + ", filiere=" + filiere
				+ "]";
	}
	
	

}