package ecole.test;

import java.util.ArrayList;
import java.util.List;

import ecole.Filiere;
import ecole.etudiant;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Créer cinq étudiants.
		//Afficher les étudiants par filière.
		
		// création filiere
		Filiere f1 = new Filiere(1, "plomberie");
		Filiere f2 = new Filiere(2, "electricit�");
		Filiere f3 = new Filiere(3, "peinture");
		List<Filiere> lesFilieres = new ArrayList<Filiere>();
		lesFilieres.add(f1);
		lesFilieres.add(f2);
		lesFilieres.add(f3);
		
		
		// création étudiant
		List<etudiant> lesEtudiants = new ArrayList<etudiant>();
		lesEtudiants.add(new etudiant("Staline", "Joseph", "12_02_1901", f1));
		lesEtudiants.add(new etudiant("Napol�on", "Bonaparte", "12_02_1901", f1));
		lesEtudiants.add(new etudiant("Bachelot", "Roseline", "12_02_1941", f1));
		lesEtudiants.add(new etudiant("Pustule", "Dede", "12_02_1981", f2));
		lesEtudiants.add(new etudiant("Benguigui", "Morice", "12_02_1961", f3));
		
		for(int i = 1; i <=3; i++){ // code de filière
			System.out.println(" ");
			System.out.println("Liste des �tudiants inscrits en " + (lesFilieres.get(i-1)).getLibelle() + " :");
			for(int j = 0; j < lesEtudiants.size(); j++){ // indice du tableau étudiant		
				if (((lesEtudiants.get(j)).getFiliere()).getCode() == i){
					System.out.println( (lesEtudiants.get(j)).getPrenom() + " " + (lesEtudiants.get(j)).getNom());
				}
			}
		}
	}
}