package ecole;

public class Filiere {
	
	private int idf;
	private int code;
	private String Libelle;
	private static int count;
	
	public Filiere(int code, String libelle) {
		super();
		this.idf = ++count;
		this.code = code;
		Libelle = libelle;
	}

	public int getIdf() {
		return idf;
	}

	public int getCode() {
		return code;
	}

	public String getLibelle() {
		return Libelle;
	}

	public String toString() {
		return super.toString() + "Filiere [idf=" + idf + ", code=" + code + ", Libelle="
				+ Libelle + "]";
	}

	public Filiere() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}