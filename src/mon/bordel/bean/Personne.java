package mon.bordel.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import mon.bordel.inter.IPersonne;

public class Personne implements IPersonne{
	
	private int idPers;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private double salaire;
	private Profil profil;
	private static int count;

	@Override
	public void affiche() {
		// TODO Auto-generated method stub
		//Je suis le directeur SAlMI Karim n� le 02 juin 1970 mon salaire est 20 000dh
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(dateNaissance);
		
		System.out.println("Je suis le " + this.profil.getLibelle()  + " " + this.nom + " " + this.prenom + " n� le " + date + " mon salaire est " + this.getSalaire() + " dh");
	}

	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		if (this.profil.getCode() == 12) {
			return (this.getSalaire()*1.2);
		} else {
			return (this.getSalaire()*1.1);
		}
	}

	public Personne(String nom, String prenom, Date dateNaissance, double salaire, Profil profil) {
		super();
		this.idPers = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.salaire = salaire;
		this.profil = profil;
	}

	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(dateNaissance);
		
		
		return "Personne [idPers=" + idPers + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + date
				+ ", salaire=" + salaire + ", profil=" + profil + "]";
	}

	public int getIdPers() {
		return idPers;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}
	
}
