package mon.bordel.bean;

public class Profil {
	
	private int idProf;
	private int code;
	private String libelle;
	private static int count;
	
	public Profil(int code, String libelle) {
		super();
		this.idProf = ++count;
		this.code = code;
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return super.toString() + "Profil [idProf=" + idProf + ", code=" + code + ", libelle=" + libelle + "]";
	}

	public Profil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdProf() {
		return idProf;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	

}
