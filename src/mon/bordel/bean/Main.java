package mon.bordel.bean;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Profil dir = new Profil(12, "Dirlo");
		Personne dirlojc = new Personne("Dus", "Jean Claude", new Date("1976/07/24"), 2000, dir);
		System.out.println(dirlojc);
		dirlojc.affiche();
		System.out.println("Mon nouveau salaire : " + dirlojc.calculerSalaire());
		
		Profil grouillo = new Profil(6, "Grouillot");
		Personne grouil = new Personne("Bebel", "Jean Paul", new Date("1946/07/24"), 1000, grouillo);
		System.out.println(grouil);
		grouil.affiche();
		System.out.println("Mon nouveau salaire : " + grouil.calculerSalaire());

	}

}
